package com.jpmc.report;

import com.jpmc.trade.api.TradeInstruction;
import com.jpmc.trade.implementation.TradeInstructionImpl;
import com.jpmc.trade.util.Trade;

/**
 * @author Sagar Badhe
 * @version 01
 *
 */
public class TradeReortGenerator {
	private static TradeInstruction tradeCounter;

	public static void main(String[] str) {
		tradeCounter = new TradeInstructionImpl();

		System.out.println(" **** Amount in USD settled incoming everyday ****");
		tradeCounter.amountSettle(Trade.S);
		System.out.println(" **** Amount in USD settled outgoing everyday ****");
		tradeCounter.amountSettle(Trade.B);
		System.out.println(" **** Ranking of entities based on incoming and outgoing amount. ****");
		tradeCounter.rankingOfEntity();

	}

}
