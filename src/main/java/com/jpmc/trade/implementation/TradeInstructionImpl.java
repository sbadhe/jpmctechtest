package com.jpmc.trade.implementation;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import com.jpmc.trade.api.TradeInstruction;
import com.jpmc.trade.pojo.Entity;
import com.jpmc.trade.util.Currency;
import com.jpmc.trade.util.DataForTest;
import com.jpmc.trade.util.Trade;

/**
 * Implementation of Trade Instruction
 * 
 * @author Sagar Badhe
 * @version 01
 *
 */
public class TradeInstructionImpl implements TradeInstruction {

	/**
	 * This method calculate and display everyday Trade
	 * 
	 * @param trade
	 *            if Buy or Sell
	 * @return amount
	 */
	@Override
	public Double amountSettle(final Trade trade) {

		Double settleAmount = new Double(0);
		List<Entity> listOfEntityByTrade = new ArrayList<Entity>();
		DataForTest.tempararyListOfAllTradeDoneInAWeek.forEach(entity -> {
			if (entity.getTrade() == trade) {
				listOfEntityByTrade.add(entity);
			}
		});
		this.everyDayTrade(listOfEntityByTrade).forEach((localDate, amount) -> {
			System.out.println(" Date of Trade " + localDate + ":  Amount :" + amount);
		});
		return settleAmount;
	}

	/**
	 * Get the Rank of each entity base on amount Sell or Buy
	 * 
	 * @return Entity and corresponding amount
	 */
	@Override
	public Map<String, Double> rankingOfEntity() {

		this.printOutInputRank(Trade.B);
		this.printOutInputRank(Trade.S);
		return null;
	}

	/**
	 * Calculate every day trade based on the number of Entity,
	 * 
	 * @param filteredEntity
	 * @return Map of amount for everyday
	 */
	private Map<LocalDate, Double> everyDayTrade(final List<Entity> filteredEntity) {

		Map<LocalDate, Double> dateAndAmoutMap = new HashMap<LocalDate, Double>();

		// Set Dummy amount for each day
		this.updateSettlementDate(filteredEntity).forEach(entity -> {
			dateAndAmoutMap.put(entity.getSettlementDate(), new Double(0));
		});
		// Calculate every day amount and update Map for date and amount
		filteredEntity.forEach(entity -> {
			dateAndAmoutMap.forEach((localDate, amount) -> {
				if (entity.getSettlementDate().equals(localDate)) {
					amount = amount + entity.getPrice() * entity.getUnit() * entity.getAgreedFix();
					dateAndAmoutMap.put(localDate, amount);
				}
			});
		});

		return dateAndAmoutMap;

	}

	/**
	 * Just to print Rank of Entity
	 * 
	 * @param trade
	 */
	private void printOutInputRank(final Trade trade) {
		int rank = 0;
		final Iterator<Entry<String, Double>> iterate = everyDayEntityTrade(
				DataForTest.tempararyListOfAllTradeDoneInAWeek, trade).entrySet().iterator();
		while (iterate.hasNext()) {
			Entry mapEntry = (Entry) iterate.next();
			System.out.println(trade.getDisplayValue() + " Entity : " + mapEntry.getKey() + " : Rank of Entity : "
					+ ++rank + " : Amount : " + mapEntry.getValue());
		}
	}

	/**
	 * Calculate Amount everyday base on buy or Sell
	 * 
	 * @param filteredEntity
	 * @param tradeOutIn
	 * @return Map of amount for each entity
	 */
	private Map<String, Double> everyDayEntityTrade(final List<Entity> filteredEntity, final Trade tradeOutIn) {

		Map<String, Double> nameAndAmoutMap = new TreeMap<String, Double>();

		this.updateSettlementDate(filteredEntity).forEach(entity -> {
			nameAndAmoutMap.put(entity.getEntityName(), new Double(0));
		});

		filteredEntity.forEach(entity -> {
			nameAndAmoutMap.forEach((entityName, amount) -> {
				if (entity.getEntityName().equals(entityName)) {
					amount = amount + entity.getPrice() * entity.getUnit() * entity.getAgreedFix();
					if (entity.getTrade() == tradeOutIn) {
						nameAndAmoutMap.put(entityName, amount);
					}

				}
			});
		});

		return nameAndAmoutMap;

	}

	/**
	 * Update Settlement Date based on Currency if it is instructed on Weekend,
	 * It will be update to next working day based on currency
	 * 
	 * @param filteredEntity
	 * @return List of updated Entity
	 */
	private List<Entity> updateSettlementDate(List<Entity> filteredEntity) {
		filteredEntity.forEach(entity -> {
			if (entity.getCurrency() == Currency.AED || entity.getCurrency() == Currency.SAR) {
				// If settlement date is fall on Friday or Saturday as it
				// weekend for AED and SAR
				if (entity.getSettlementDate().getDayOfWeek() == DayOfWeek.FRIDAY) {
					entity.setSettlementDate(entity.getSettlementDate().plusDays(2)); // Add
																						// 2
																						// days
																						// if
																						// Friday
				} else if (entity.getSettlementDate().getDayOfWeek() == DayOfWeek.SATURDAY) {
					entity.setSettlementDate(entity.getSettlementDate().plusDays(1)); // Add
																						// 1
																						// days
																						// if
																						// Saturday
				}

			} else {
				/*
				 * If settlement date is fall on Saturday or Sunday as it
				 * weekend for all other Currency
				 */
				if (entity.getSettlementDate().getDayOfWeek() == DayOfWeek.SATURDAY) {
					entity.setSettlementDate(entity.getSettlementDate().plusDays(2)); // Add
																						// 2
																						// days
																						// if
																						// Saturday
				} else if (entity.getSettlementDate().getDayOfWeek() == DayOfWeek.SUNDAY) {
					entity.setSettlementDate(entity.getSettlementDate().plusDays(1)); // Add
																						// 1
																						// days
																						// if
																						// Sunday
				}

			}

		});

		return filteredEntity;
	}

}
