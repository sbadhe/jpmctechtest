package com.jpmc.trade.pojo;

import java.time.LocalDate;

import com.jpmc.trade.util.Currency;
import com.jpmc.trade.util.Trade;

/**
 * 
 * @author Sagar Badhe
 *
 */
public class Entity {

	private String entityName;
	private Trade trade;
	private Currency currency;
	private double agreedFix;
	private LocalDate instructionDate;
	private LocalDate settlementDate;
	private int unit;
	private double price;

	public Entity(String entityName, Trade trade, Currency currency, double agreedFix, LocalDate instructionDate,
			LocalDate settlementDate, int unit, double price) {
		super();
		this.entityName = entityName;
		this.trade = trade;
		this.currency = currency;
		this.agreedFix = agreedFix;
		this.instructionDate = instructionDate;
		this.settlementDate = settlementDate;
		this.unit = unit;
		this.price = price;
	}

	public String getEntityName() {
		return entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	public Trade getTrade() {
		return trade;
	}

	public void setTrade(Trade trade) {
		this.trade = trade;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	public double getAgreedFix() {
		return agreedFix;
	}

	public void setAgreedFix(double agreedFix) {
		this.agreedFix = agreedFix;
	}

	public LocalDate getInstructionDate() {
		return instructionDate;
	}

	public void setInstructionDate(LocalDate instructionDate) {
		this.instructionDate = instructionDate;
	}

	public LocalDate getSettlementDate() {
		return settlementDate;
	}

	public void setSettlementDate(LocalDate settlementDate) {
		this.settlementDate = settlementDate;
	}

	public int getUnit() {
		return unit;
	}

	public void setUnit(int unit) {
		this.unit = unit;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

}
