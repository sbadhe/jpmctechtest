package com.jpmc.trade.api;

import java.util.Map;

import com.jpmc.trade.util.Trade;

/**
 * Interface For Trading Instructions
 * 
 * @author Sagar Badhe
 * @version 01
 *
 */
public interface TradeInstruction {

	Double amountSettle(Trade trade);

	Map<String, Double> rankingOfEntity();
}
