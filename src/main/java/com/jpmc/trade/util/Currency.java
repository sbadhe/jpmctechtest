package com.jpmc.trade.util;

public enum Currency {

	AED("Emirates"),
	SAR("Saudi"),
	SGP("Singapore"),
	USD("US"),
	INR("India"),
	EUR("Europe");
	
	private String countryName;
	
	Currency(String countryName)
	{
		this.countryName = countryName;
	}
	
	
	public String getCountryName()
	{
		return this.countryName;
	}
	
	
}
