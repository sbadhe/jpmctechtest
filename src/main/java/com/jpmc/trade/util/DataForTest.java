package com.jpmc.trade.util;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import com.jpmc.trade.pojo.Entity;

/**
 * 
 * Just a Test Data for generating Report
 * 
 * @author Sagar Badhe
 *
 */
public class DataForTest {

	public static List<Entity> tempararyListOfAllTradeDoneInAWeek = Arrays.asList(
			new Entity("foo", Trade.B, Currency.SGP, 0.50, LocalDate.of(2017, 3, 23), LocalDate.of(2017, 3, 24), 200,
					100.25),
			new Entity("bar", Trade.S, Currency.AED, 0.22, LocalDate.of(2017, 3, 24), LocalDate.of(2017, 3, 25), 450,
					150.5),
			new Entity("bar", Trade.B, Currency.SAR, 0.22, LocalDate.of(2017, 3, 25), LocalDate.of(2017, 3, 26), 451,
					150.5),
			new Entity("foo", Trade.S, Currency.SGP, 0.50, LocalDate.of(2017, 3, 26), LocalDate.of(2017, 3, 27), 201,
					100.25),
			new Entity("bar", Trade.B, Currency.INR, 0.22, LocalDate.of(2017, 3, 27), LocalDate.of(2017, 3, 28), 452,
					150.5),
			new Entity("bar", Trade.S, Currency.AED, 0.22, LocalDate.of(2017, 3, 28), LocalDate.of(2017, 3, 29), 400,
					150.5),
			new Entity("foo", Trade.B, Currency.SGP, 0.50, LocalDate.of(2017, 3, 30), LocalDate.of(2017, 3, 31), 150,
					100.25),
			new Entity("bar", Trade.S, Currency.AED, 0.22, LocalDate.of(2017, 3, 23), LocalDate.of(2017, 4, 01), 350,
					150.5),
			new Entity("bar", Trade.B, Currency.AED, 0.22, LocalDate.of(2017, 3, 24), LocalDate.of(2017, 4, 02), 351,
					150.5),
			new Entity("foo", Trade.S, Currency.SGP, 0.50, LocalDate.of(2017, 3, 25), LocalDate.of(2017, 4, 03), 202,
					100.25),
			new Entity("bar", Trade.B, Currency.AED, 0.22, LocalDate.of(2017, 3, 31), LocalDate.of(2017, 4, 04), 402,
					150.5),
			new Entity("foo", Trade.S, Currency.INR, 0.50, LocalDate.of(2017, 4, 01), LocalDate.of(2017, 4, 05), 192,
					100.25),
			new Entity("bar1", Trade.B, Currency.USD, 0.50, LocalDate.of(2017, 4, 02), LocalDate.of(2017, 4, 06), 194,
					100.25),
			new Entity("foo", Trade.S, Currency.EUR, 0.50, LocalDate.of(2017, 4, 03), LocalDate.of(2017, 4, 07), 184,
					100.25),
			new Entity("bar", Trade.B, Currency.EUR, 0.50, LocalDate.of(2017, 4, 04), LocalDate.of(2017, 4, 8), 122,
					100.25),
			new Entity("foo", Trade.B, Currency.SGP, 0.50, LocalDate.of(2017, 4, 05), LocalDate.of(2017, 4, 9), 123,
					100.25)

	);

}
