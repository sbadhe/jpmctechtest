package com.jpmc.trade.util;

public enum Trade {

	B ("Outgoing"),  
	S ("Incoming");
	
	private String displayName;
	
	Trade(String displayName)
	{
		this.displayName = displayName;
	}
	
	public String getDisplayValue(){
		return displayName;
	}
	
}
